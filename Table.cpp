//
// Created by dredd on 17/10/2019.
//

#include "Table.h"

double Table::estimationCouverture() {
    int m = this->hauteur;
    float v = 1.0;
    for (int i = 0; i < this->largeur; i++) {
        v = v * (1 - m / (float) this->config.getN());
        m = this->config.getN() * (1 - exp(-m / (float) this->config.getN()));
    }
    double couverture = 100 * (1 - v);
    return couverture;
}

long Table::estimationTaille() {
    // il y a hauteur Chaines dans la tables, chaque chaine contient 2 index, chaque index fait 8 octets
    return this->hauteur * 2 * 8;
}

void Table::displayEstimation() {
    cout << "Couverture : " << this->estimationCouverture() << endl;
    cout << "Taille (en octet) : " << this->estimationTaille() << endl;
}