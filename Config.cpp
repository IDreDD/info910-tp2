//
// Created by root on 08/10/2019.
//

#include <iostream>
#include "Config.h"
using namespace std;

void Config::toString() {
    cout<<"Alphabet: "<< this->alphabet<<endl;
    cout<<"Taille Min: "<< this->taille_min<<endl;
    cout<<"Taille Max: "<< this->taille_max<<endl;
    cout<<"N: "<< this->N<<endl;
    cout<<"T: ";
    for (vector <long long>::iterator it=this->T.begin(); it!=this->T.end(); it++){
        cout << *it << " ";
    }
    cout << endl;
}

long Config::getN() const {
    return N;
}

const vector<long long> &Config::getT() {
    return T;
}

const string &Config::getAlphabet() const {
    return alphabet;
}

int Config::getTailleMin() const {
    return taille_min;
}

int Config::getTailleMax() const {
    return taille_max;
}

char *Config::getType() const {
    return type;
}


