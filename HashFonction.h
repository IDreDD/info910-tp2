//
// Created by root on 14/10/2019.
//

#ifndef TP2_HASHFONCTION_H
#define TP2_HASHFONCTION_H


#include "Config.h"

class HashFonction {

public:
    HashFonction();

    unsigned char * hasher (char *message, Config c);

    void hash_MD5(const char *c, unsigned char *H);

    void hash_SHA1(const char *c, unsigned char *H);
};


#endif //TP2_HASHFONCTION_H
