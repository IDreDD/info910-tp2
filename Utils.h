//
// Created by root on 08/10/2019.
//

#ifndef TP2_COMPROMIS_H
#define TP2_COMPROMIS_H

#include <vector>
#include "Config.h"
#include "Chaine.h"
#include "Table.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

class Utils {
    Config config;


public:
    Utils(){};

    Utils(Config _config):config(_config){
    };

    void setConfig( Config config);

    long long h2i(unsigned char* y, int t);

    char* i2c( long long indx);

    long long i2i ( long long indx, int t);

    Chaine nouvelleChaine(long long indx, int largeur);

    long long indexAleatoire();

    Table creerTable(int largeur, int hauteur);

    int inverse(Table table, int largeur, int hauteur, unsigned char* h, char* clair);

    int rechercheDicho( Table table, int hauteur, long long idx, int *a, int *b);

    int rechercheExhaustive( Table table, int hauteur, long long idx, int *a, int *b);

    bool verifieCandidat( unsigned char* h, int t, long long idx, char* clair);

    void sauvegarder(ostream &output, Table T);

    Table charger(istream &input);
};


#endif //TP2_COMPROMIS_H
