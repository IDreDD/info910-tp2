#include <iostream>
#include <fstream>
#include "Config.h"
#include "Utils.h"
#include "HashFonction.h"
#include "Chaine.h"
#include <vector>
#include "Interface.h"


using namespace std;

int main(int argc, char* argv[]){
    Interface inter;
    if (string(argv[1])=="--hash"){
        inter.Hash(argc, argv);
    }else if (string(argv[1])=="--crack"){
        inter.Crack(argc, argv);
    } else if (string(argv[1])=="--create-table"){
        inter.CreateTable(argc, argv);
    }else if (string(argv[1])=="--help"){
        inter.Help();
    }else{
        inter.Default();
    };
    /*
    char* type  = argv[1];
    char * message = argv[2];
    int tmin = atoi(argv[3]);
    int tmax = atoi(argv[4]);
    string alph;
    Config c;

    if (argc==5) {
        c = Config (tmin, tmax, type);
    }else if (argc ==6) {
        alph = argv[5];
        c= Config (alph, tmin, tmax, type);
    }else{
        cout<<"Invalide params"<<endl;
        cout<<"<type de cryptage> <message> <taille min> <taille max> <alphabet>" << endl;
        cout << "param <alphabet> optionnel, alphabet par defaut 'abcdefghijklcmnopqrstuvwxyz'" << endl;
        return 0;
    }
    cout << c << endl;
    c.toString();
    Utils utils (c);

    char *testCharI2d = utils.i2c(142678997);
    cout  << "test" << testCharI2d << endl;

    long long t = utils.i2i(142678997,1);
    cout << t << endl;


    Table table = utils.creerTable(100, 100);
    cout << table << endl;
    table.displayEstimation();

    ofstream output( "testO");
    utils.sauvegarder(output ,table);
    HashFonction hsh;
    char * clair;
    unsigned char *h = hsh.hasher( message, c);
    int res = utils.inverse(table, table.largeur, table.hauteur, h, clair);
    cout << "finish with result " << res << " text " << clair << endl;


    ifstream input( "testO");
    if (input.good()){
        Table tableretour = utils.charger(input);
        cout << tableretour << endl;
    }
     */

    return 0;
}