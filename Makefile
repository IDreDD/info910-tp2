CC=g++
CFLAGS=-O3 -lcrypto -lssl
LDFLAGS=
EXEC=hash

all: 
	$(CC) -o a.out main.cpp Interface.h Interface.cpp HashFonction.cpp HashFonction.h Config.h Config.cpp Chaine.h Chaine.cpp Table.h Table.cpp Utils.h Utils.cpp  $(CFLAGS)

hash:
	$(CC) -o hash.out main.cpp HashFonction.cpp HashFonction.h Config.h Config.cpp Chaine.h Chaine.cpp Table.h Table.cpp Utils.h Utils.cpp  $(CFLAGS)

clean:
	rm -rf *.o *.out