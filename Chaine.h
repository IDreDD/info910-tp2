//
// Created by dredd on 16/10/2019.
//

#ifndef INFO910_TP2_CHAINE_H
#define INFO910_TP2_CHAINE_H

#include <iostream>

using  namespace std;
class Chaine {
public:
    long long start;
    long long end;
    int largeur;

    Chaine(long long _start, long long _end, int _largeur):start(_start), end(_end), largeur(_largeur){
    };

    bool operator <(const Chaine &b) const{
        return end < b.end;
    }

    bool operator <=(const Chaine &b) const{
        return end <= b.end;
    }

    bool operator >=(const Chaine &b) const{
        return end >= b.end;
    }

    bool operator >(const Chaine &b) const{
        return end > b.end;
    }

    friend ostream& operator << (ostream& os, const Chaine& c);

};

inline ostream& operator << (ostream& os, const Chaine& c){
    os << c.start << " " << c.end;
    return os;
}


#endif //INFO910_TP2_CHAINE_H
