//
// Created by root on 18/10/2019.
//
#include <iostream>
#include "Interface.h"
#include "HashFonction.h"
#include "Utils.h"
#include "Config.h"
#include "Table.h"
using namespace std;

Interface::Interface() {};

void Interface::Hash(int argc, char **argv) {
    if( !argc >= 4)
    {
        this->Default();
        return;
    }
    HashFonction hf;
    unsigned char *H;
    Config c (-1, -1, argv[3]);
    if (string(argv[3])== "MD5"){
        H = new unsigned char[16];
        H = hf.hasher(argv[2],c);
        cout << "Hash  de " << argv[2] << " en " << argv[3] << " donne : ";
        for (int i = 0; i < 16; i++){
            cout << hex << (int)H[i];
        }
        cout <<dec << endl;
    }
    else if (string(argv[3])== "SHA1") {
        H = new unsigned char[20];
        H= hf.hasher(argv[2],c);
        cout << "Hash  de " << argv[2] << " en " << argv[3] << " donne : ";
        for (int i = 0; i < 20; i++){
            cout << hex << (int)H[i];
        }
        cout <<dec << endl;
    }
}

void Interface::Crack(int argc, char **argv) {
    if(!argc >= 4){
        this->Default();
        return;
    }
    Table table;
    Utils utils;

    ifstream input(argv[2]);
    if (input.good()){
        table = utils.charger(input);
    } else{
        cout << "input not good " << endl;
    }
    utils.setConfig(table.config);
    char * clair;
    unsigned char *H = (unsigned char*) argv[3];
    int res = utils.inverse(table, table.largeur, table.hauteur, H, clair);
    if(res == 1)
        cout << "Solution trouvé - " << clair << endl;
    else
        cout << "Aucune solution trouvé, " << res << " candidats testés" << endl;
}

void Interface::CreateTable(int argc, char **argv) {
    if(!argc >= 7){
        this->Default();
        return;
    }
    int hauteurTable = atoi(argv[2]);
    int largeurTable = atoi(argv[3]);
    char * mode = argv[4];
    int tailleMin = atoi(argv[5]);
    int tailleMax = atoi(argv[6]);
    string alphabet;
    if(argc >= 8)
        alphabet = (string) argv[7];
    else
        alphabet = "abcdefghijklmnopqrstuvwxyz";

    Config config(alphabet, tailleMin, tailleMax, mode);
    Utils utils(config);
    Table table = utils.creerTable(largeurTable, hauteurTable);
    ofstream output;
    if (argc >= 9)
        output.open(argv[8]);
    else
        output.open("table");

    if (output.good()){
        utils.sauvegarder(output, table);
        output.close();
        cout << "Table sauvegarder " << endl;
    }
    else {
        cout << " output not good " << endl;
    }

}

void Interface::Default() {
    cout << "Erreur de parametres, --help pour avoir la liste des commandes et options utilisables" << endl;

}

void Interface::Help() {
    cout << " options de lancement : " << endl;
    cout << "--hash <S> <Mode>" << endl;
    cout << "      S => String a hasher" << endl;
    cout << "      Mode => MD5 ou SHA1 pour choisir le mode de hashage" << endl;
    cout << endl;
    cout << "--crack <FileName> <H>" << endl;
    cout << "      FileName => Nom du fichier qui contient la table" << endl;
    cout << "      H => Hash a craquer" << endl;
    cout << "--create-table <HauteurTable> <LarrgeurTable>  <Mode> <TailleMin> <TailleMax> <alphabet> <destination>" << endl;
    cout << "-     HauteurTable => Hauteur de la table a créer" << endl;
    cout << "-     LargeurTable => Largeur de la table a créer" << endl;
    cout << "      Mode => MD5 ou SHA1 pour choisir le mode de hashage" << endl;
    cout << "      TailleMin => Taille minimum des mots" << endl;
    cout << "      TailleMax => Taille maximum des mots" << endl;
    cout << "      alphabet => alphabet utilisé" << endl;
    cout << "-     Destination => Nom du fichier où sauvegarder la table" << endl;

}

