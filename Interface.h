//
// Created by root on 18/10/2019.
//

#ifndef TP2_INTERFACE_H
#define TP2_INTERFACE_H

using namespace std;

class Interface {


public:

    Interface();

    void Hash (int argc, char* argv[]);

    void Crack(int argc, char *argv[]);

    void CreateTable(int argc, char **argv);

    void Default();

    void Help();
};


#endif //TP2_INTERFACE_H
