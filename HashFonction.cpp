//
// Created by root on 14/10/2019.
//

using namespace std;


#include <cstring>
#include "HashFonction.h"
#include "Utils.h"
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <iostream>


unsigned char * HashFonction::hasher(char *message, Config c) {
    unsigned char *H;
    if (strcmp(c.getType(), "MD5")==0){
        H = new unsigned char[16];
        hash_MD5(message, H);
    }
    else if (strcmp(c.getType(), "SHA1")==0) {
        H = new unsigned char[20];
        hash_SHA1(message, H);
    }
    return H;

}

HashFonction::HashFonction() {}

void HashFonction::hash_MD5(const char *c, unsigned char *H) {
    MD5((unsigned char*)c, strlen(c), H);

}

void HashFonction::hash_SHA1(const char *c, unsigned char *H) {
    SHA1((unsigned char*)c, strlen(c), H);

}

