//
// Created by root on 08/10/2019.
//
#include <string>
#include <cmath>
#include <iostream>
#include <vector>


#ifndef TP2_CONFIG_H
#define TP2_CONFIG_H

using namespace std;

class Config {
    string alphabet;
    int taille_min;
    int taille_max;
    long long N;
    vector <long long> T;
    char *type;



public:

    Config(string _alphabet, int _taille_min, int _taille_max, char *_type): alphabet(_alphabet), taille_max(_taille_max), taille_min(_taille_min), type(_type){
        this->N=0;
        for (int i = taille_min; i<= taille_max; i++){
            this->N+=pow(alphabet.size(),i);
        }
        for (int i = taille_min; i<=taille_max; i++){
            this->T.push_back(pow(alphabet.size(),i));
        }
    }

    Config( int _taille_min, int _taille_max, char *_type):taille_max(_taille_max),taille_min(_taille_min), type(_type){
        this->N=0;
        this->alphabet="abcdefghijklmnopqrstuvwxyz";
        cout<< "size of " << this->alphabet.size()<< endl;
        for (int i = taille_min; i<= taille_max; i++){
            this->N+=pow(alphabet.size(),i);
        }
       for (int i = taille_min; i<=taille_max; i++){
            this->T.push_back(pow(alphabet.size(),i));
        }
    }

    Config (){
        this->alphabet="";
        this->taille_min=-1;
        this->taille_max=-1;
        this->N = -1;
    }

    void toString();

    long getN() const;

    const vector<long long> &getT();

    const string &getAlphabet() const;

    int getTailleMin() const;

    int getTailleMax() const;

    char *getType() const;

    friend ostream& operator << (ostream& os, const Config& config);

};

inline ostream& operator << (ostream& os, const Config& config){
    os << config.getType() << "\n" << config.getTailleMin()<< "\n" << config.getTailleMax() << "\n" << config.getAlphabet() << "\n";
    return os;
}



#endif //TP2_CONFIG_H
