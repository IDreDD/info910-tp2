//
// Created by dredd on 17/10/2019.
//

#ifndef INFO910_TP2_TABLE_H
#define INFO910_TP2_TABLE_H

#include <vector>
#include <iostream>
#include "Chaine.h"
#include "Config.h"

using namespace std;
class Table {
public:
    Config config;
    vector<Chaine> content;
    int hauteur;
    int largeur;

    Table(){};

    Table(Config _config, int _largeur, int _hauteur, vector<Chaine> _content):config(_config), largeur(_largeur), hauteur(_hauteur), content(_content){
    };

    double estimationCouverture();

    long estimationTaille();

    void displayEstimation();

    friend ostream& operator << (ostream& os, const Table& t);

};

inline ostream& operator << (ostream& os, const Table& t){
    os << t.config <<t.largeur << "x" << t.hauteur << "\n";
    for (int i = 0; i < t.hauteur; ++i) {
        os << t.content.at(i) << "\n";
    }
    return os;
}

#endif //INFO910_TP2_TABLE_H
