Alexander PEREZ
Quentin CODELUPI

PARTIE 1

Fonctionnelle, nous pouvons créer des tables et les sauvegarder dans des fichier. Et les charger pour pouvoir les réutiliser ailleur.
Le seul problème vient de l'aléatoire de la table,
il n'y a pas de seed pour le random c++ c'est à dire que les nombres donné par random seront les même d'une exécution à l'autre.

Pour l'exécution de cette partie se fait en utilisant les paramètres suivant afin de créer et sauvegarder une table:

    --create-table <hauteur_table> <largeur_table> <hash_type> <taille_min> <taille_max> <alphabet (optionnal)> <destination (optionnal)>

Si aucune destination n'est entrée, la table sera sauvegarder dans le fichier "table" dans le dossier de l'exécution.
Si aucun alphabet n'est entré, l'alphabet "abcdefghijklmnoqrstuvwxyz" sera utilisé.

PARTIE 2 -

Fonctionnelle, nous pouvons lancé le crack d'un hash grace a la recherche dichotomique ou exhaustive, en chargeant une table depuis un fichier.
On calcul aussi une estimation de la table.
Les résultats sont relativement aproximatifs.

Pour l'exécution de cette partie il faut utiliser les paramètres suivants :

    --crack <path_table> <hash_to_crack>

Une exécution pour générer le Hash d'un texte clair est disponible pour les tests

    --hash <text_clair> <hash_type>



Questions :

Q6 - Pourquoi l'ajout du paramètre t améliore la couverture de la table ?
    L'ajout du paramètre t apporte plude de variation dans les indices et permet donc de générer plus d'indices différents.
    En répartissant mieux les indices, on augmente la couverture de la table.

Q10 - Estimer la complexité de la recherche dans une table arc en ciel

