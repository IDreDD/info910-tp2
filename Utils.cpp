//
// Created by root on 08/10/2019.
//
#include <vector>
#include <algorithm>
#include <cstring>
#include "Utils.h"
#include "HashFonction.h"

void Utils::setConfig( Config config){
    this->config = config;
}

/// H2I - I2C - I2I

long long Utils::h2i(unsigned char *y, int t) {
    unsigned char newY[sizeof(y)];
    for (int i; i< sizeof(y); i++){
        newY[i]=y[i];
    }
    uint64_t* ptr = (uint64_t*) newY;
    uint64_t a = ptr[0];
    return (a+t)%this->config.getN();
}

char* Utils::i2c(long long indx) {
    int tailleDuMot = 0;
    char *res;

    vector<char> vector;
    if (indx < this->config.getT()[0]){
        tailleDuMot=this->config.getTailleMin();
    }else if (indx >= this->config.getN()){
        return nullptr;
    }else {
        long long somme =this->config.getT()[0];
        for (int i = 0; i < this->config.getT().size() - 1; i++) {
            if (indx >= somme && indx < somme+this->config.getT()[i + 1]) {
                tailleDuMot = i + this->config.getTailleMin() + 1;
            }
            somme += this->config.getT()[i + 1];
        }
    }

    indx = indx - pow(this->config.getAlphabet().size(), tailleDuMot - 1);

    //Set up for return
    for (int i = 0; i< tailleDuMot; i++){
            char lettre = this->config.getAlphabet()[indx % this->config.getAlphabet().size()];
            vector.push_back(lettre);
            indx = indx / this->config.getAlphabet().size();
    }
    reverse(vector.begin(),vector.end());
    res = new char[vector.size()];

    for (int i = 0; i < vector.size(); i++){
        res[i]=vector.at(i);
    }

    return res;
}


long long Utils::i2i(long long indx, int t) {
    char *c = i2c(indx);
    HashFonction hsh;
    unsigned char *H;
    H = hsh.hasher(c, this->config);
    long long res = h2i(H,t);
    return res;
}

/// Création Chaine / Tables

Chaine Utils::nouvelleChaine(long long indx, int largeur){
    // Idée : appeler largeur fois i2i et garder uniquement le premier et le dernier
    long long start = indx;
    long long end = indx;
    for (int i = 1; i < largeur; ++i) {
        end = i2i(end, i);
    }
    Chaine res(start, end, largeur);
    return res;
}

long long Utils::indexAleatoire(){
    // Idée : Génération de nombres aléatoires et combinaison de ceux-ci pour avoir un nombre aléatoire assez grand (64bits)
    // TODO : Seed the random
    uint64_t n;
    unsigned long n1 = rand();
    unsigned long n2 = rand();
    n = ( (uint64_t) n2 ) + ( ( (uint64_t) n1 ) << 32 );
    return (long long) n%this->config.getN();

}

Table Utils::creerTable(int largeur, int hauteur){
    // Idée : Créer hauteur fois une nouvelle Chaine de largeur largeur, chaque Chaine commencant par un index aléatoire.
    vector<Chaine> content;
    for (int i = 0; i < hauteur; ++i) {
        long long startIndex = indexAleatoire(); // index Aleatoire
        Chaine c = nouvelleChaine(startIndex, largeur);
        content.push_back(c);
    }
    sort(content.begin(), content.end());
    Table table(this->config, largeur, hauteur, content);
    return table;
}


/// Inversement

int Utils::inverse(Table table, int largeur, int hauteur, unsigned char* h, char* clair){

    int nbCandidat = 0;
    long long idx;
    int a;
    int b;
    for (int t = largeur - 1; t > 0; t--) {
        cout << t << " " << this->config.getN() <<  endl;
        idx = h2i(h, t);
        for (int i = t + 1; i < largeur; ++i) {
            idx = i2i(idx, i);
        }
        //Si trouvé
        if (rechercheDicho(table, hauteur, idx, &a, &b) > 0 ){
            cout << a << " to " << b <<endl;
            for (int i = a; i <= b; ++i) {
                //Si bon candidat
                if (verifieCandidat(h, t, table.content.at(i).start, clair))
                    return 1;
                else
                    nbCandidat++;
            }
        }

    }
    return nbCandidat;
}

//doit retourner -1 si pas trouver et 1 si trouver
// Idée : Puisque les tables sont ordonnée par ordre croissant de dernière colonne
int Utils::rechercheDicho( Table table, int hauteur, long long idx, int *a, int *b){
    int inferieur = 0;
    int superieur = table.hauteur;
    int m = (int)((inferieur + superieur)/2);
    while ( (inferieur < superieur) && (table.content.at(m).end != idx) ){
        if (idx < table.content.at(m).end ){
            superieur = m - 1;
        }
        else{
            inferieur = m + 1;
        }
        m = (int)((inferieur + superieur)/2);
    }

    *a = m;
    *b = m;
    if((m < hauteur) && idx == table.content.at(m).end){

        cout << m << " " << a << " " << b << " * " << *a << " " << *b <<endl;
        while(*a > 0 &&idx == table.content.at(*a).end ){
            *a = *a - 1;
            cout << "in boucle a " << *a << endl;
        }
        cout << " set a " << endl;
        while ( *b < hauteur -1 &&idx == table.content.at(*b).end ){
            *b = *b + 1;
        }
        cout << " set b" << endl;
        return 1;
    }
    else{
        return -1;
    }

}

int Utils::rechercheExhaustive(Table table, int hauteur, long long idx, int *a, int *b) {

    *a = -1;
    *b = -1;
    int i = 0;

    while ( i < hauteur){
        if( *a!= -1 && table.content.at(i).end != idx){
            return 1;
        }
        else if (*a != -1 && table.content.at(i).end == idx ){
            *b++;
        }
        else if(table.content.at(i).end == idx){
            *a = i;
            *b = i;
        }
        i ++;

    }
    return -1;
}


bool Utils::verifieCandidat( unsigned char* h, int t, long long idx, char* clair){
    cout << "verif candidat" <<endl;
    for (int i = 1; i < t; i++) {
        idx = i2i(idx, i);
    }
    clair = i2c(idx);
    cout << clair << endl;
    HashFonction hsh;
    unsigned char *h2 = hsh.hasher(clair, this->config);
    return h2 == h;
}

////// Lecture / Ecriture de ficher
void Utils::sauvegarder(ostream &output, Table t){
    //Idée : On écrit la config de la table puis sa taille et pour finir, on parcours la table et écrit les chaines dans le fichier.
    //Config
    output << t.config.getType() << endl;
    output << t.config.getTailleMin() << endl;
    output << t.config.getTailleMax() << endl;
    output << t.config.getAlphabet() << endl;
    //Taille
    output << t.largeur << " " << t.hauteur << endl;
    //Contenu
    for (int i = 0; i < t.hauteur; ++i) {
        output << t.content.at(i) << endl;
    }

}

Table Utils::charger(istream &input) {
    string str;

    //Config
    getline(input, str);
    char* type = new char[str.length() + 1];
    strcpy(type, str.c_str());
    getline(input, str);
    int tailleMin = stoi(str);
    getline(input, str);

    int tailleMax = stoi(str);
    getline(input, str);

    string alphabet = str;
    Config config(alphabet, tailleMin, tailleMax, type);

    //Taille
    getline(input, str);
    istringstream ss(str);
    int largeur;
    int hauteur;
    ss >> largeur >> hauteur;

    //Contenu
    vector<Chaine> content;
    getline(input, str);
    for (int i = 0; i < hauteur; ++i) {
        istringstream iss(str);
        long long start;
        long long end;
        iss >> start >> end;
        Chaine chaine(start, end, largeur);
        content.push_back(chaine);
        getline(input, str);
    }

    Table table(config, largeur, hauteur, content);
    return table;
}




